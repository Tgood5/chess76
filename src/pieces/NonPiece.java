package pieces;

import chess.Board;
/**
 * placeholder for empty board squares.
 *
 * @author Thurgood Kilper
 * @author Brandon Kennedy
*/
public class NonPiece implements Piece{

	public String name;

	@Override
	public boolean move(String coordinates, Board board) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public boolean getTeam() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean populateTrajectory(int row, int col, Board mainBoard, int[][] controlBoard) {
		// TODO Auto-generated method stub
		return false;
	}

}
