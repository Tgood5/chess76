package pieces;

import chess.Board;
/**
 * The Queen. Moves and attacks in any direction.
 *
 * @author Thurgood Kilper
 * @author Brandon Kennedy
 */
public class Queen implements Piece{

	/**
     * The color of the piece. False = black
     */
	public boolean white;

	/**
     * The name of the piece
     */
	public String name;

	@Override
	public boolean move(String coordinates, Board board) {

		String initialPosition = coordinates.substring(0, 2);
		String newPosition = coordinates.substring(3,5);
		int oldCol = (int)(initialPosition.charAt(0)-97);
		int newCol = (int)(newPosition.charAt(0)-97);
		int oldRow = 8-(int)(initialPosition.charAt(1)-48);
		int newRow = 8-(int)(newPosition.charAt(1)-48);



		//Making sure that the new coordinates are not out of bounds:
		if(newCol > 7 || newCol < 0 || newRow > 7 || newRow < 0){
			//System.out.println("Out of bounds");
			return false; //Out of bounds
		}

		//Handling diagonal moves:
		if((oldCol != newCol && oldRow != newRow) && (Math.abs(newRow - oldRow) == Math.abs(newCol - oldCol))){
			if(oldCol > newCol && oldRow > newRow){ //Going Up and Left
				int i = oldRow - 1;
				int j = oldCol - 1;

				while(i !=newRow && j != newCol){
					if(!(board.boardLayout[i][j] instanceof NonPiece)){
						 System.out.println("Up and Left Jump on coordinates: " + i + j);
						return false; //Illegal move, jumping over pieces.
					}
					i--; j--;
				}
				/*Placing (or attacking) in the new spot, clearing the old spot:
				if(board.boardLayout[newRow][newCol].getTeam() == board.boardLayout[oldRow][oldCol].getTeam()){
					return false; //Attacking your own team
				}*/
				board.boardLayout[newRow][newCol] = board.boardLayout[oldRow][oldCol];
				board.boardLayout[oldRow][oldCol] = board.clearLayout[oldRow][oldCol];
				return true;

			}else if(oldCol < newCol && oldRow > newRow){ //Going Up and Right
				int i = oldRow -1;
				int j = oldCol + 1;
				while(i != newRow && j != newCol){
					if(!(board.boardLayout[i][j] instanceof NonPiece)){
						System.out.println("Up and Right jump on coordinates: " + i + j);
						return false; //Illegal move, jumping over pieces.
					}
					i--; j++;
				}

				//Placing (or attacking) in the new spot, clearing the old spot:
				if(board.boardLayout[newRow][newCol].getTeam() == board.boardLayout[oldRow][oldCol].getTeam()){
					return false; //Attacking your own team
				}
				board.boardLayout[newRow][newCol] = board.boardLayout[oldRow][oldCol];
				board.boardLayout[oldRow][oldCol] = board.clearLayout[oldRow][oldCol];
				return true;

			}else if(oldCol < newCol && oldRow < newRow){ //Going Down and Right

				int i = oldRow + 1;
				int j = oldCol + 1;

				while(i != newRow && j != newCol){
						 if(!(board.boardLayout[i][j] instanceof NonPiece)){
							 System.out.println("Down and Right jump on coordinates: " + i + j);
							 return false; //Illegal move, jumping over pieces.
						 }

					i++; j++;
				}

				/*Placing (or attacking) in the new spot, clearing the old spot:
				if(board.boardLayout[newRow][newCol].getTeam() == board.boardLayout[oldRow][oldCol].getTeam()){
					return false; //Attacking your own team
				}*/
				board.boardLayout[newRow][newCol] = board.boardLayout[oldRow][oldCol];
				board.boardLayout[oldRow][oldCol] = board.clearLayout[oldRow][oldCol];
				return true;

			}else if(oldCol > newCol && oldRow < newRow){ //Going Down and Left

				int i = oldRow + 1;
				int j = oldCol - 1;

				while(i != newRow && j != newCol){
						 if(!(board.boardLayout[i][j] instanceof NonPiece)){
							 System.out.println("Down and Left jump on coordinates: " + i + j);
							 return false; //Illegal move, jumping over pieces.
						 }
						 i++; j--;
				}

				//Placing (or attacking) in the new spot, clearing the old spot:
				/*if(board.boardLayout[newRow][newCol].getTeam() == board.boardLayout[oldRow][oldCol].getTeam()){

					System.out.println("Attack Down and Left");
					return false; //Attacking your own team
				}*/
				board.boardLayout[newRow][newCol] = board.boardLayout[oldRow][oldCol];
				board.boardLayout[oldRow][oldCol] = board.clearLayout[oldRow][oldCol];
				return true;
			}

		}else{ //Handling non-diagonal moves.
			//Horizontal move:
			if(oldRow == newRow){
				int temp = oldCol;
					if(oldCol > newCol){ //Going left
						temp = oldCol - 1;
						while(temp != newCol){
							if(!(board.boardLayout[newRow][temp] instanceof NonPiece)){
								return false; //Jumping over other pieces
							}
							temp--;
						}
					}else if(oldCol < newCol){ //Going right
						temp = oldCol + 1;
						while(temp != newCol){
							if(!(board.boardLayout[newRow][temp] instanceof NonPiece)){
								return false; //Jumping over other pieces
							}
							temp++;
						}
					}
				//Placing (or attacking) in the new spot, clearing the old spot:
				/*if(board.boardLayout[newRow][newCol].getTeam() == board.boardLayout[oldRow][oldCol].getTeam()){
					return false; //Attacking your own team
				}*/
				board.boardLayout[newRow][newCol] = board.boardLayout[oldRow][oldCol];
				board.boardLayout[oldRow][oldCol] = board.clearLayout[oldRow][oldCol];
				return true;
			}

			//Vertical move:
			if(oldCol == newCol){
				int temp1 = oldRow;
				if(oldRow > newRow){
					temp1 = oldRow - 1;
					while(temp1 != newRow){ //Going up the board
						if(!(board.boardLayout[temp1][oldCol] instanceof NonPiece)){
							return false; //Jumping over other pieces
						}
						temp1--;
					}
				}else if(oldRow < newRow){
					temp1 = oldRow + 1;
					while(temp1 != newRow){ //Going down the board
						if(!(board.boardLayout[temp1][oldCol] instanceof NonPiece)){
							return false; //Jumping over other pieces
						}
						temp1++;
					}

				}
				//Placing (or attacking) in the new spot, clearing the old spot:

				/*if(board.boardLayout[newRow][newCol].getTeam() == board.boardLayout[oldRow][oldCol].getTeam()){
					return false; //Attacking your own team
				}*/
				board.boardLayout[newRow][newCol] = board.boardLayout[oldRow][oldCol];
				board.boardLayout[oldRow][oldCol] = board.clearLayout[oldRow][oldCol];
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public boolean getTeam() {
		// TODO Auto-generated method stub
		return white;
	}

	@Override
	public boolean populateTrajectory(int row, int col, Board mainBoard, int[][] controlBoard) {
		int oppTeam;
		//dont populate trajectory if takeable by defenders
		if(white){
			oppTeam=2;
			if(controlBoard[row][col]==2)
				return false;
		}else{
			oppTeam=1;
			if(controlBoard[row][col]==1)
				return false;
		}
		boolean collision = false;
		int rowTemp = row+1;
		int colTemp = col+1;

		//Populating the diagonal trajectory (Down and Right):
			while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
					&& mainBoard.boardLayout[rowTemp][colTemp] instanceof NonPiece
					&& controlBoard[rowTemp][colTemp]!=oppTeam){
				controlBoard[rowTemp][colTemp] = controlBoard[row][col];
				rowTemp++;
				colTemp++;
			}
			if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
				controlBoard[rowTemp][colTemp] = controlBoard[row][col];
				//Checking for a collision with a King:
				if((mainBoard.boardLayout[rowTemp][colTemp] instanceof King)
						&& mainBoard.boardLayout[rowTemp][colTemp].getTeam()!=white){
					collision = true;
					if(rowTemp+1<8 && colTemp+1<8)
						controlBoard[rowTemp+1][colTemp+1]=controlBoard[row][col];
				}
			}
				//Resetting rowTemp and colTemp:
				rowTemp = row-1;
				colTemp = col-1;

		//Populating the diagonal trajectory (Up and Left):

			while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
					&& mainBoard.boardLayout[rowTemp][colTemp] instanceof NonPiece
					&& controlBoard[rowTemp][colTemp]!=oppTeam){
				controlBoard[rowTemp][colTemp] = controlBoard[row][col];
				rowTemp--;
				colTemp--;
			}
			if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
				controlBoard[rowTemp][colTemp] = controlBoard[row][col];
				//Checking for a collision with a King:
				if((mainBoard.boardLayout[rowTemp][colTemp] instanceof King)
						&& mainBoard.boardLayout[rowTemp][colTemp].getTeam()!=white){
					collision = true;
					if(rowTemp-1>=0 && colTemp-1>=0)
						controlBoard[rowTemp-1][colTemp-1]=controlBoard[row][col];
				}
			}
			//Resetting rowTemp and colTemp:
			rowTemp = row-1;
			colTemp = col+1;

		//Populating the diagonal trajectory (Up and Right):

			while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
					&& mainBoard.boardLayout[rowTemp][colTemp] instanceof NonPiece
					&& controlBoard[rowTemp][colTemp]!=oppTeam){
				controlBoard[rowTemp][colTemp] = controlBoard[row][col];
				rowTemp--;
				colTemp++;
			}
			if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
				controlBoard[rowTemp][colTemp] = controlBoard[row][col];
				//Checking for a collision with a King:
				if((mainBoard.boardLayout[rowTemp][colTemp] instanceof King)
						&& mainBoard.boardLayout[rowTemp][colTemp].getTeam()!=white){
					collision = true;
					if(rowTemp-1>=0 && colTemp+1<8)
						controlBoard[rowTemp-1][colTemp+1]=controlBoard[row][col];
				}
			}

				//Resetting rowTemp and colTemp:
				rowTemp = row+1;
				colTemp = col-1;

		//Populating the diagonal trajectory (Down and Left):

			while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
					&& mainBoard.boardLayout[rowTemp][colTemp] instanceof NonPiece
					&& controlBoard[rowTemp][colTemp]!=oppTeam){
				controlBoard[rowTemp][colTemp] = controlBoard[row][col];
				rowTemp++;
				colTemp--;
			}
			if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
				controlBoard[rowTemp][colTemp] = controlBoard[row][col];
				//Checking for a collision with a King:
				if((mainBoard.boardLayout[rowTemp][colTemp] instanceof King)
						&& mainBoard.boardLayout[rowTemp][colTemp].getTeam()!=white){
					collision = true;
					if(rowTemp+1<8 && colTemp-1>=0)
						controlBoard[rowTemp+1][colTemp-1]=controlBoard[row][col];
				}
			}

			rowTemp = row+1;
			colTemp = col+1;

		//Populating the horizontal trajectory right:
		while((mainBoard.boardLayout[row][colTemp] instanceof NonPiece)
				&& colTemp < 8 && controlBoard[row][colTemp]!=oppTeam){
			controlBoard[row][colTemp] = controlBoard[row][col];
			colTemp++;
		}
		if(colTemp < 8){
			controlBoard[row][colTemp] = controlBoard[row][col];
			//Checking to see if there was a collision with a King:
			if((mainBoard.boardLayout[row][colTemp] instanceof King)
					&& mainBoard.boardLayout[row][colTemp].getTeam()!=white){
				collision = true;
				if(colTemp+1<8)
					controlBoard[row][colTemp+1]=controlBoard[row][col];
			}
		}

		//Populating the vertical trajectory down:
		while((mainBoard.boardLayout[rowTemp][col] instanceof NonPiece)
				&& rowTemp < 8 && controlBoard[rowTemp][col]!=oppTeam){
			controlBoard[rowTemp][col] = controlBoard[row][col];
			rowTemp++;
		}
		if(rowTemp < 8){
			controlBoard[rowTemp][col] = controlBoard[row][col];
			//Checking to see if there was a collision with a King:
			if((mainBoard.boardLayout[rowTemp][col] instanceof King)
					&& mainBoard.boardLayout[rowTemp][col].getTeam()!=white){
				collision = true;
				if(rowTemp+1<8)
					controlBoard[rowTemp+1][col]=controlBoard[row][col];
			}
		}
		//reset row and col temp position
		rowTemp = row-1;
		colTemp = col-1;

		//Populating the horizontal trajectory left:
		while(colTemp >=0 && (mainBoard.boardLayout[row][colTemp] instanceof NonPiece)
				 && controlBoard[row][colTemp]!=oppTeam){
			controlBoard[row][colTemp] = controlBoard[row][col];
			colTemp--;
		}
		if(colTemp >=0){
			controlBoard[row][colTemp] = controlBoard[row][col];
			//Checking to see if there was a collision with a King:
			if((mainBoard.boardLayout[row][colTemp] instanceof King)
					&& mainBoard.boardLayout[row][colTemp].getTeam()!=white){
				collision = true;
				if(colTemp-1>=0)
					controlBoard[row][colTemp-1]=controlBoard[row][col];
			}
		}

		//Populating the vertical trajectory up:
		while(rowTemp >=0 && (mainBoard.boardLayout[rowTemp][col] instanceof NonPiece)
				&& controlBoard[rowTemp][col]!=oppTeam){
			controlBoard[rowTemp][col] = controlBoard[row][col];
			rowTemp--;
		}
		if(rowTemp >=0){
			controlBoard[rowTemp][col] = controlBoard[row][col];
			//Checking to see if there was a collision with a King:
			if((mainBoard.boardLayout[rowTemp][col] instanceof King)
					&& mainBoard.boardLayout[rowTemp][col].getTeam()!=white){
				collision = true;
				if(rowTemp-1>=0)
					controlBoard[rowTemp-1][col]=controlBoard[row][col];
			}
		}

		return collision;

	}

}
