package pieces;

import chess.Board;
/**
 * The Knight. Moves and attacks 2 spaces horizontally or vertically, then one space in a perpendicular direction.
 * can pass over other pieces
 *
 * @author Thurgood Kilper
 * @author Brandon Kennedy
 */
public class Knight implements Piece{

	/**
     * The color of the piece. False = black
     */
	public boolean white;

	/**
     * The name of the piece
     */
	public String name;

	@Override
	public boolean move(String coordinates, Board board) {
		//parse input string
				String initialPosition = coordinates.substring(0, 2);
				String newPosition = coordinates.substring(3);
				int oldCol = (int)(initialPosition.charAt(0)-97);
				int newCol = (int)(newPosition.charAt(0)-97);
				int oldRow = 8-(int)(initialPosition.charAt(1)-48);
				int newRow = 8-(int)(newPosition.charAt(1)-48);

				boolean legal=false;
													//check out of bounds
				if(newCol > 7 || newCol < 0 || newRow > 7 || newRow < 0)
					return false;
													//check if new space is open or attacking
				if(white){
					if(!(board.boardLayout[newRow][newCol] instanceof NonPiece)){
						if(board.boardLayout[newRow][newCol].getTeam())
							return false;
					}
				}else{
					if(!(board.boardLayout[newRow][newCol] instanceof NonPiece)){
						if(!(board.boardLayout[newRow][newCol].getTeam()))
							return false;
					}
				}
													//moving, can jump other pieces
				if(newCol==oldCol+2 || newCol==oldCol-2){
					if(newRow==oldRow+1 || newRow==oldRow-1){
						legal=true;
					}
				}else if(newRow==oldRow+2 || newRow==oldRow-2){
					if(newCol==oldCol+1 || newCol==oldCol-1){
						legal=true;
					}
				}

				if(legal){							//if the move was legal, move the piece, otherwise throw error
					board.boardLayout[newRow][newCol]=board.boardLayout[oldRow][oldCol];
					board.boardLayout[oldRow][oldCol]=board.clearLayout[oldRow][oldCol];
				}
				return legal;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public boolean getTeam() {
		// TODO Auto-generated method stub
		return white;
	}

	@Override
	public boolean populateTrajectory(int row, int col, Board mainBoard, int[][] controlBoard) {
		//dont populate trajectory if takeable by defenders
				if(white){
					if(controlBoard[row][col]==2)
						return false;
				}else{
					if(controlBoard[row][col]==1)
						return false;
				}
				boolean collision = false;

				//update threat squares if not blocked, not out of bounds
				//return true if threatening enemy king
				if(row+2<8){
					if(col+1<8){
						controlBoard[row+2][col+1] = controlBoard[row][col];
						if(mainBoard.boardLayout[row+2][col+1] instanceof King && mainBoard.boardLayout[row+2][col+1].getTeam()!=white)
							collision=true;
					}
					if(col-1>=0){
						controlBoard[row+2][col-1] = controlBoard[row][col];
						if(mainBoard.boardLayout[row+2][col-1] instanceof King && mainBoard.boardLayout[row+2][col-1].getTeam()!=white)
							collision=true;
					}
				}
				if(row-2>=0){
					if(col+1<8){
						controlBoard[row-2][col+1] = controlBoard[row][col];
						if(mainBoard.boardLayout[row-2][col+1] instanceof King && mainBoard.boardLayout[row-2][col+1].getTeam()!=white)
							collision=true;
					}
					if(col-1>=0){
						controlBoard[row-2][col-1] = controlBoard[row][col];
						if(mainBoard.boardLayout[row-2][col-1] instanceof King && mainBoard.boardLayout[row-2][col-1].getTeam()!=white)
							collision=true;
					}
				}
				if(row+1<8){
					if(col+2<8){
						controlBoard[row+1][col+2] = controlBoard[row][col];
						if(mainBoard.boardLayout[row+1][col+2] instanceof King && mainBoard.boardLayout[row+1][col+2].getTeam()!=white)
							collision=true;
					}
					if(col-2>=0){
						controlBoard[row+1][col-2] = controlBoard[row][col];
						if(mainBoard.boardLayout[row+1][col-2] instanceof King && mainBoard.boardLayout[row+1][col-2].getTeam()!=white)
							collision=true;
					}
				}
				if(row-1>=0){
					if(col+2<8){
						controlBoard[row-1][col+2] = controlBoard[row][col];
						if(mainBoard.boardLayout[row-1][col+2] instanceof King && mainBoard.boardLayout[row-1][col+2].getTeam()!=white)
							collision=true;
					}
					if(col-2>=0){
						controlBoard[row-1][col-2] = controlBoard[row][col];
						if(mainBoard.boardLayout[row-1][col-2] instanceof King && mainBoard.boardLayout[row-1][col-2].getTeam()!=white)
							collision=true;
					}
				}

				return collision;
	}

}
