package pieces;

import chess.Board;
/**
 * The Pawn. Moves forward one square. Attacks diagonally one square.
 * <p>
 * On its first move it can go two squares forward.
 * Even if it moves two squares, an opposing pawn can capture it by attacking the empty space behind it (enpassant).
 * Upon reaching the opposite end of the board, it is promoted to a piece of the player's choosing.
 * <p>
 *
 * @author Thurgood Kilper
 * @author Brandon Kennedy
 */
public class Pawn implements Piece{

	/**
     * The color of the piece. False = black
     */
	public boolean white;

	/**
     * The name of the piece
     */
	public String name;

	/**
	 * holds the turn number for which this pawn can be taken by enpassant
	 */
	public int enpTurn;

	@Override
	public boolean move(String coordinates, Board board) {
		//parse input string
		String initialPosition = coordinates.substring(0, 2);
		String newPosition = coordinates.substring(3);
		int oldCol = (int)(initialPosition.charAt(0)-97);
		int newCol = (int)(newPosition.charAt(0)-97);
		int oldRow = 8-(int)(initialPosition.charAt(1)-48);
		int newRow = 8-(int)(newPosition.charAt(1)-48);

		boolean legal=false;
		enpTurn=-1;
											//check for coordinates out of bounds:
		if(newCol > 7 || newCol < 0 || newRow > 7 || newRow < 0)
			return false;

		if(white){							//white starts at bottom
			if(newRow==oldRow-1){
				if(newCol==oldCol){			//legal move 1 space forward, if space is free
					if(board.boardLayout[newRow][newCol] instanceof NonPiece){
						legal=true;
					}
				}else if(newCol==oldCol+1 || newCol==oldCol-1){
											//attacking, check for black piece
					if(!(board.boardLayout[newRow][newCol] instanceof NonPiece) && board.boardLayout[newRow][newCol].getTeam()!=white){
						legal=true;
					}else if(board.boardLayout[oldRow][newCol] instanceof Pawn){
											//check pawn for enpassant, take if legal
						Pawn tmp = (Pawn) board.boardLayout[oldRow][newCol];
						if(tmp.enpTurn==board.turnCount-1 && !tmp.getTeam()){
							board.boardLayout[newRow][newCol]=board.boardLayout[oldRow][oldCol];
							board.boardLayout[oldRow][oldCol]=board.clearLayout[oldRow][oldCol];
							board.boardLayout[oldRow][newCol]=board.clearLayout[oldRow][newCol];
							return true;
						}
					}
				}
			}else if(newRow==4&&oldRow==6){	//first move can be two spaces if spaces are free, set en passant status
				if(newCol==oldCol){
					if(board.boardLayout[newRow][newCol] instanceof NonPiece && board.boardLayout[newRow+1][newCol] instanceof NonPiece){
						legal=true;
						enpTurn=board.turnCount;
					}
				}
			}//end white pawn
		}else{								//black starts at top
			if(newRow==oldRow+1){
				if(newCol==oldCol){			//legal move 1 space down, if space is free
					if(board.boardLayout[newRow][newCol] instanceof NonPiece){
						legal=true;
					}
				}else if(newCol==oldCol+1 || newCol==oldCol-1){
											//attacking, check for white piece
					if(!(board.boardLayout[newRow][newCol] instanceof NonPiece) && board.boardLayout[newRow][newCol].getTeam()!=white){
						legal=true;
					}else if(board.boardLayout[oldRow][newCol] instanceof Pawn){
											//check pawn for enpassant, take if legal
						Pawn tmp = (Pawn) board.boardLayout[oldRow][newCol];
						if(tmp.enpTurn==board.turnCount-1 && tmp.getTeam()){
							board.boardLayout[newRow][newCol]=board.boardLayout[oldRow][oldCol];
							board.boardLayout[oldRow][oldCol]=board.clearLayout[oldRow][oldCol];
							board.boardLayout[oldRow][newCol]=board.clearLayout[oldRow][newCol];
							return true;
						}
					}
				}
			}else if(newRow==3&&oldRow==1){	//first move can be two spaces
				if(newCol==oldCol){
					if(board.boardLayout[newRow][newCol] instanceof NonPiece && board.boardLayout[newRow-1][newCol] instanceof NonPiece){
						legal=true;
						enpTurn=board.turnCount;
					}
				}
			}
		}//end black pawn

		if(legal){							//if the move was legal, move the piece, otherwise throw error
			board.boardLayout[newRow][newCol]=board.boardLayout[oldRow][oldCol];
			board.boardLayout[oldRow][oldCol]=board.clearLayout[oldRow][oldCol];
		}
		if(newRow==0 || newRow==7){			//check for promotion
			char promote='0';
			String newName;
			if(white)
				newName="w";
			else
				newName="b";
			if (coordinates.length()>=7)
				promote = coordinates.charAt(6);
			switch(promote){
				case 'N': 	Knight pnight = new Knight();
							pnight.white = white;
							newName+=promote;
							pnight.name = newName;
							board.boardLayout[newRow][newCol]=pnight;
							break;
				case 'B': 	Bishop pbishop = new Bishop();
							pbishop.white = white;
							newName+=promote;
							pbishop.name = newName;
							board.boardLayout[newRow][newCol]=pbishop;
							break;
				case 'R': 	Rook prook = new Rook();
							prook.white = white;
							newName+=promote;
							prook.name = newName;
							board.boardLayout[newRow][newCol]=prook;
							break;
				default: 	Queen pqueen = new Queen();
							//Default to queen promotion for none or invalid promotion input
							pqueen.white = white;
							newName+='Q';
							pqueen.name = newName;
							board.boardLayout[newRow][newCol]=pqueen;
							break;
			}
		}//end promotion
		return legal;

	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public boolean getTeam() {
		// TODO Auto-generated method stub
		return white;
	}

	@Override
	public boolean populateTrajectory(int row, int col, Board mainBoard, int[][] controlBoard) {
		//dont populate trajectory if takeable by defenders
				if(white){
					if(controlBoard[row][col]==2)
						return false;
				}else{
					if(controlBoard[row][col]==1)
						return false;
				}
				boolean collision=false;
				if(white){
					if(row-1>=0){
						if((mainBoard.boardLayout[row-1][col] instanceof NonPiece))
							controlBoard[row-1][col] = controlBoard[row][col];
						if(col+1<8){
							if(!(mainBoard.boardLayout[row-1][col+1] instanceof NonPiece) && mainBoard.boardLayout[row-1][col+1].getTeam()!=white){
								controlBoard[row-1][col+1] = controlBoard[row][col];
								if(mainBoard.boardLayout[row-1][col+1] instanceof King)
									collision=true;
							}
						}
						if(col-1>=0){
							if(!(mainBoard.boardLayout[row-1][col-1] instanceof NonPiece) && mainBoard.boardLayout[row-1][col-1].getTeam()!=white){
								controlBoard[row-1][col-1] = controlBoard[row][col];
								if(mainBoard.boardLayout[row-1][col-1] instanceof King)
									collision=true;
							}
						}
					}
				}else{
					if(row+1<8){
						if((mainBoard.boardLayout[row+1][col] instanceof NonPiece))
							controlBoard[row+1][col] = controlBoard[row][col];
						if(col+1<8){
							if(!(mainBoard.boardLayout[row+1][col+1] instanceof NonPiece) && mainBoard.boardLayout[row+1][col+1].getTeam()!=white){
								controlBoard[row+1][col+1] = controlBoard[row][col];
								if(mainBoard.boardLayout[row+1][col+1] instanceof King)
									collision=true;
							}
						}
						if(col-1>=0){
							if(!(mainBoard.boardLayout[row+1][col-1] instanceof NonPiece) && mainBoard.boardLayout[row+1][col-1].getTeam()!=white){
								controlBoard[row+1][col-1] = controlBoard[row][col];
								if(mainBoard.boardLayout[row+1][col-1] instanceof King)
									collision=true;
							}
						}
					}
				}
				return collision;
	}

}
