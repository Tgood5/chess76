package pieces;

import chess.Board;
/**
 * The King. Moves and attacks one space in any direction.
 * able to castle if it and a rook have not yet moved and the space between them is empty
 *
 * @author Thurgood Kilper
 * @author Brandon Kennedy
 */
public class King implements Piece{

	/**
     * The color of the piece. False = black
     */
	public boolean white;

	/**
     * The name of the piece
     */
	public String name;

	/**
     * determines if castling is allowed
     */
	public boolean canCastle=true;

	@Override
	public boolean move(String coordinates, Board board) {
		//parse input string
				String initialPosition = coordinates.substring(0, 2);
				String newPosition = coordinates.substring(3);
				int oldCol = (int)(initialPosition.charAt(0)-97);
				int newCol = (int)(newPosition.charAt(0)-97);
				int oldRow = 8-(int)(initialPosition.charAt(1)-48);
				int newRow = 8-(int)(newPosition.charAt(1)-48);

				boolean legal=false;
													//check out of bounds
				if(newCol > 7 || newCol < 0 || newRow > 7 || newRow < 0)
					return false;
													//check if new space is open or attacking
				if(white){
					if(!(board.boardLayout[newRow][newCol] instanceof NonPiece)){
						if(board.boardLayout[newRow][newCol].getTeam())
							return false;
					}
				}else{
					if(!(board.boardLayout[newRow][newCol] instanceof NonPiece)){
						if(!(board.boardLayout[newRow][newCol].getTeam()))
							return false;
					}
				}
				if(canCastle){						//CASTLING
					if(newRow==oldRow && newCol==oldCol+2){
												//castle right
						if(board.boardLayout[oldRow][oldCol+3] instanceof Rook
								&& board.boardLayout[oldRow][oldCol+1] instanceof NonPiece){
							Rook tmp = (Rook) board.boardLayout[oldRow][oldCol+3];
							if (tmp.canCastle){
								board.boardLayout[newRow][newCol]=board.boardLayout[oldRow][oldCol];
								board.boardLayout[oldRow][oldCol]=board.clearLayout[oldRow][oldCol];
								board.boardLayout[newRow][newCol-1]=board.boardLayout[oldRow][oldCol+3];
								board.boardLayout[oldRow][oldCol+3]=board.clearLayout[oldRow][oldCol+3];
								return true;
							}
						}
					}else if(newRow==oldRow && newCol==oldCol-2){
												//castle left
						if(board.boardLayout[oldRow][oldCol-4] instanceof Rook
								&& board.boardLayout[oldRow][oldCol-1] instanceof NonPiece
								&& board.boardLayout[oldRow][oldCol-3] instanceof NonPiece){
							Rook tmp = (Rook) board.boardLayout[oldRow][oldCol-4];
							if (tmp.canCastle){
								board.boardLayout[newRow][newCol]=board.boardLayout[oldRow][oldCol];
								board.boardLayout[oldRow][oldCol]=board.clearLayout[oldRow][oldCol];
								board.boardLayout[newRow][newCol+1]=board.boardLayout[oldRow][oldCol-4];
								board.boardLayout[oldRow][oldCol-4]=board.clearLayout[oldRow][oldCol-4];
								return true;
							}
						}
					}
				}//END CASTLING

													//can move 1 space in any direction
				if(newCol==oldCol+1 || newCol==oldCol-1){
					if(newRow==oldRow || newRow==oldRow+1 || newRow==oldRow-1){
						legal=true;
					}
				}else if(newRow==oldRow+1 || newRow==oldRow-1){
					if(newCol==oldCol || newCol==oldCol+1 || newRow==oldCol-1){
						legal=true;
					}
				}

				if(legal){							//if the move was legal, move the piece, otherwise throw error
					board.boardLayout[newRow][newCol]=board.boardLayout[oldRow][oldCol];
					board.boardLayout[oldRow][oldCol]=board.clearLayout[oldRow][oldCol];
					canCastle=false;
				}
				return legal;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public boolean getTeam() {
		// TODO Auto-generated method stub
		return white;
	}

	@Override
	public boolean populateTrajectory(int row, int col, Board mainBoard, int[][] controlBoard) {
		int teamnum;
		if(white)
			teamnum=1;
		else
			teamnum=2;
		//project on enemies in range
		if((row-1>=0) && (col-1>=0)){
			if(!(mainBoard.boardLayout[row-1][col-1] instanceof NonPiece) && mainBoard.boardLayout[row-1][col-1].getTeam()!=white){
				controlBoard[row-1][col-1]=teamnum;
			}
		}else if(row -1 >= 0){
			if(!(mainBoard.boardLayout[row-1][col] instanceof NonPiece) && mainBoard.boardLayout[row-1][col].getTeam()!=white){
				controlBoard[row-1][col]=teamnum;
			}
		}else if(row - 1 >= 0 && col + 1 < 8){
			if(!(mainBoard.boardLayout[row-1][col+1] instanceof NonPiece) && mainBoard.boardLayout[row-1][col+1].getTeam()!=white){
				controlBoard[row-1][col+1]=teamnum;
			}
		}else if(col + 1 < 8){
			if(!(mainBoard.boardLayout[row][col+1] instanceof NonPiece) && mainBoard.boardLayout[row][col+1].getTeam()!=white){
				controlBoard[row][col+1]=teamnum;
			}
		}else if(row + 1 < 8 && col + 1 < 8){
			if(!(mainBoard.boardLayout[row+1][col+1] instanceof NonPiece) && mainBoard.boardLayout[row+1][col+1].getTeam()!=white){
				controlBoard[row+1][col+1]=teamnum;
			}
		}else if(row + 1 < 8){
			if(!(mainBoard.boardLayout[row+1][col] instanceof NonPiece) && mainBoard.boardLayout[row+1][col].getTeam()!=white){
				controlBoard[row+1][col]=teamnum;
			}
		}else if(row + 1 < 8 && col - 1 >= 0){
			if(!(mainBoard.boardLayout[row+1][col-1] instanceof NonPiece) && mainBoard.boardLayout[row+1][col-1].getTeam()!=white){
				controlBoard[row+1][col-1]=teamnum;
			}
		}else if(col -1 >= 0){
			if(!(mainBoard.boardLayout[row][col-1] instanceof NonPiece) && mainBoard.boardLayout[row][col-1].getTeam()!=white){
				controlBoard[row][col-1]=teamnum;
			}
		}
		return false;

	}

}
