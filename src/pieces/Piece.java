package pieces;

import chess.Board;
/**
 * @author Thurgood Kilper
 * @author Brandon Kennedy
 * <p>
 * interface for chess piece functions
 *
*/
public interface Piece {
	/**
	 * Moves the piece according to the user's coordinates.
	 *
	 *@param coordinates	A string given by the user specifying the movement of the piece. (e.g: b2 b4)
	 *@param board The instance of the board used in the current game.
	 *@return 	boolean returns true if move was successful.
	 */
	public boolean move(String coordinates, Board board);

	/**
	 * This method returns the piece name. The name is used in printing the Board
	 *
	 * @return String name of the current chess piece.
	 */
	public String getName();

	/**
	 * This method returns the team (i.e. black or white).
	 *
	 * @return boolean value returns white(true) or black(false).
	 */
	public boolean getTeam();

	/**
	 * method for determining threatened squares
	 * use to determine check
	 *
	 * @param row	An integer value of the X coordinate
	 * @param col 	An integer value of the Y coordinate
	 * @param mainBoard The current board setup
	 * @param controlBoard The board that is used to map all the trajectories.
	 * @return success
	 */
	public boolean populateTrajectory(int row, int col, Board mainBoard, int[][] controlBoard);
}
