
package chess;

import java.util.Scanner;


/**
 * @author Thurgood Kilper
 * @author Brandon Kennedy
 *<p>
 *This is the main method for gameplay.
 *
 *creates a game board, then loops through player turns until
 *a winner is decided
*/
public class Chess {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		//initialize and print the board
		Board board = new Board();
		Board prevTurn = new Board();
		board.initialize();
		board.print();
		System.out.println();

		boolean legal=false;
		boolean drawRequest=false;

		String moveResult;
		//input string
		String userInput = "";

		//gameplay loop
		do{
			if(board.turnCount != 1){
				//get user input
				userInput = sc.nextLine();
				userInput.toLowerCase();

				//check for resign
				if(userInput.equals("resign"))
					return;

				//check for draw
				if(userInput.equals("draw") && drawRequest==true)
					return;

				//check bad input
				if(userInput.length()<5){
					System.out.print("Illegal move, try again: ");
					continue;
				}

				//-48 for digits, -97 for lowercase letters, 8- to compensate for board orientation
				int oldRank = 8-(int)(userInput.charAt(1)-48);
				int oldFile = (int)(userInput.charAt(0)-97);

				//check out of bounds input
				if(oldRank > 7 || oldRank < 0 || oldFile > 7 || oldFile < 0){
					System.out.print("Illegal move, try again: ");
					continue;
				}

				//Making sure that black pieces can't be moved on white's turn and vice versa:
				if(board.turnCount%2 == 1 && board.boardLayout[oldRank][oldFile].getTeam() == true){
					System.out.print("Illegal move, try again: ");
					continue;
				}else if(board.turnCount%2 == 0 && board.boardLayout[oldRank][oldFile].getTeam() == false){
					System.out.print("Illegal move, try again: ");
					continue;
				}else{
					//----------------MOVING----------------
					//store board before moving in case of illegal move
					prevTurn=board;
					legal = board.boardLayout[oldRank][oldFile].move(userInput, board);
					//check for illegal piece movement
					if(!legal){
						System.out.println("Illegal move, try again: ");
						continue;
					}
					//if movement was legal, determine game status
					moveResult=board.isKingChecked();
					//System.out.println(moveResult);
					switch(moveResult){
						case "blackChecked"	:
											//illegal if black is left in check as a result of own move
											if(board.turnCount%2 == 1){
												legal=false;
											}
											System.out.println("Check");
											break;

						case "whiteChecked" :
											//illegal if white is left in check as a result of own move
											if(board.turnCount%2 == 0){
												legal=false;
											}
											System.out.println("Check");
											break;

						case "bothChecked"	:
											//illegal move
											legal=false;
											break;
						case "blackCheckMate" :
											if(board.turnCount%2 == 1){
												legal=false;
												break;
											}
											//black is checkmated, white wins
											System.out.println("White wins");
											return;
						case "whiteCheckMate" :
											if(board.turnCount%2 == 0){
												legal=false;
												break;
											}
											//white is checkmated, white wins
											System.out.println("Black wins");
											return;
						case "StaleMate"	:
											//Stalemate...
											System.out.println("Stalemate");
											return;

						default:
											//continue normally if no conditions are met
											break;

					}//---------------END MOVE CHECK-----------------
					if(!legal){
						board=prevTurn;
						System.out.println("Illegal move, try again: ");
						continue;
					}
					//check for draw request
					String[] inputTokens = userInput.split(" ");
					if(inputTokens.length>=3){
						if(inputTokens[2].equals("draw?"))
							drawRequest=true;
					}else
						drawRequest=false;

					//Printing the board:
					System.out.println();
					board.print();
					System.out.println();
				}

			}//end if turncount != 1

			if(board.turnCount%2 == 1){
				System.out.print("White's move: ");
			}else{
				System.out.print("Black's move: ");
			}

			board.turnCount++;
		}while(sc.hasNextLine());

		sc.close();
	}
}
