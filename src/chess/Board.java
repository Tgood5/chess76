

package chess;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.NonPiece;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;

/**
 * @author Thurgood Kilper
 * @author Brandon Kennedy
 *
 * The Board class initializes and prints the board,
 * and determines the state of the board (e.g: check, checkmate)
 */

public class Board {
	//pieces board
	public Piece[][] boardLayout = new Piece[9][9];
	//empty board (holds square color)
	public Piece[][] clearLayout = new Piece[8][8];
	public int turnCount=1;

	public Board(){}

	public Board(Piece [][] boardLayout, Piece[][] clearLayout){

		this.boardLayout = boardLayout;
		this.clearLayout = clearLayout;
		this.turnCount = 1;
	}

	/**
	 * This method initializes the board with pieces at their
	 * starting positions.
	 * It also initializes an empty board.
	 */
	public void initialize(){

		//Initializing clearLayout with empty board's black and white markings:
		for(int m = 0; m < 8; m++){
			for(int n = 0; n < 8; n++){
				if((m%2 == 1 && n%2 == 0) || (m%2 == 0 && n%2 == 1)){
					NonPiece temp = new NonPiece();
					temp.name = "##";
					clearLayout[m][n] = temp;
				}else{
					NonPiece temp1 = new NonPiece();
					temp1.name = "  ";
					clearLayout[m][n] = temp1;
				}
			}
		}

		//Initializing boardLayout with pieces in original positions:
		Rook bRl = new Rook();
			bRl.name = "bR";
		Rook bRr = new Rook();
			bRr.name = "bR";
		Rook wRl = new Rook();
			wRl.white = true;
			wRl.name = "wR";
		Rook wRr = new Rook();
			wRr.white = true;
			wRr.name = "wR";

		boardLayout[0][0] = bRl;
		boardLayout[0][7] = bRr;
		boardLayout[7][0] = wRl;
		boardLayout[7][7] = wRr;

		Knight bNl = new Knight();
			bNl.name = "bN";
		Knight bNr = new Knight();
			bNr.name = "bN";
		Knight wNl = new Knight();
			wNl.white = true;
			wNl.name = "wN";
		Knight wNr = new Knight();
			wNr.white = true;
			wNr.name = "wN";

		boardLayout[0][1] = bNl;
		boardLayout[0][6] = bNr;
		boardLayout[7][1] = wNl;
		boardLayout[7][6] = wNr;

		Bishop bBl = new Bishop();
			bBl.name = "bB";
		Bishop bBr = new Bishop();
			bBr.name = "bB";
		Bishop wBl = new Bishop();
			wBl.white = true;
			wBl.name = "wB";
		Bishop wBr = new Bishop();
			wBr.white = true;
			wBr.name = "wB";

		boardLayout[0][2] = bBl;
		boardLayout[0][5] = bBr;
		boardLayout[7][2] = wBl;
		boardLayout[7][5] = wBr;

		Queen bQ = new Queen();
			bQ.name = "bQ";
		Queen wQ = new Queen();
			wQ.white = true;
			wQ.name = "wQ";

		boardLayout[0][3] = bQ;
		boardLayout[7][3] = wQ;

		King bK = new King();
			bK.name = "bK";
		King wK = new King();
			wK.white = true;
			wK.name = "wK";

		boardLayout[0][4] = bK;
		boardLayout[7][4] = wK;

		//Populating the board with pawns:

			Pawn bp0 = new Pawn();
				bp0.name = "bp";
			Pawn bp1 = new Pawn();
				bp1.name = "bp";
			Pawn bp2 = new Pawn();
				bp2.name = "bp";
			Pawn bp3 = new Pawn();
				bp3.name = "bp";
			Pawn bp4 = new Pawn();
				bp4.name = "bp";
			Pawn bp5 = new Pawn();
				bp5.name = "bp";
			Pawn bp6 = new Pawn();
				bp6.name = "bp";
			Pawn bp7 = new Pawn();
				bp7.name = "bp";
			Pawn wp0 = new Pawn();
				wp0.white = true;
				wp0.name = "wp";
			Pawn wp1 = new Pawn();
				wp1.white = true;
				wp1.name = "wp";
			Pawn wp2 = new Pawn();
				wp2.white = true;
				wp2.name = "wp";
			Pawn wp3 = new Pawn();
				wp3.white = true;
				wp3.name = "wp";
			Pawn wp4 = new Pawn();
				wp4.white = true;
				wp4.name = "wp";
			Pawn wp5 = new Pawn();
				wp5.white = true;
				wp5.name = "wp";
			Pawn wp6 = new Pawn();
				wp6.white = true;
				wp6.name = "wp";
			Pawn wp7 = new Pawn();
				wp7.white = true;
				wp7.name = "wp";

			boardLayout[1][0] = bp0;
			boardLayout[1][1] = bp1;
			boardLayout[1][2] = bp2;
			boardLayout[1][3] = bp3;
			boardLayout[1][4] = bp4;
			boardLayout[1][5] = bp5;
			boardLayout[1][6] = bp6;
			boardLayout[1][7] = bp7;

			boardLayout[6][0] = wp0;
			boardLayout[6][1] = wp1;
			boardLayout[6][2] = wp2;
			boardLayout[6][3] = wp3;
			boardLayout[6][4] = wp4;
			boardLayout[6][5] = wp5;
			boardLayout[6][6] = wp6;
			boardLayout[6][7] = wp7;


		//Populating the empty section of the board:

		for(int j = 1; j < 8; j+=2){

			NonPiece temp = new NonPiece();
			NonPiece temp1 = new NonPiece();

			temp.name = "##";
			temp1.name = "  ";


			boardLayout[2][j] = temp;
			boardLayout[2][j-1] = temp1;
			boardLayout[4][j] = temp;
			boardLayout[4][j-1] = temp1;
			boardLayout[3][j-1] = temp;
			boardLayout[3][j] = temp1;
			boardLayout[5][j-1] = temp;
			boardLayout[5][j] = temp1;

		}

		//Populating the board coordinates:


		NonPiece ver1 = new NonPiece();
		ver1.name = "1";
		NonPiece ver2 = new NonPiece();
		ver2.name = "2";
		NonPiece ver3 = new NonPiece();
		ver3.name = "3";
		NonPiece ver4 = new NonPiece();
		ver4.name = "4";
		NonPiece ver5 = new NonPiece();
		ver5.name = "5";
		NonPiece ver6 = new NonPiece();
		ver6.name = "6";
		NonPiece ver7 = new NonPiece();
		ver7.name = "7";
		NonPiece ver8 = new NonPiece();
		ver8.name = "8";


		boardLayout[0][8] = ver8;
		boardLayout[1][8] = ver7;
		boardLayout[2][8] = ver6;
		boardLayout[3][8] = ver5;
		boardLayout[4][8] = ver4;
		boardLayout[5][8] = ver3;
		boardLayout[6][8] = ver2;
		boardLayout[7][8] = ver1;

		NonPiece horA = new NonPiece();
		horA.name = " a";
		NonPiece horB = new NonPiece();
		horB.name = " b";
		NonPiece horC = new NonPiece();
		horC.name = " c";
		NonPiece horD = new NonPiece();
		horD.name = " d";
		NonPiece horE = new NonPiece();
		horE.name = " e";
		NonPiece horF = new NonPiece();
		horF.name = " f";
		NonPiece horG = new NonPiece();
		horG.name = " g";
		NonPiece horH = new NonPiece();
		horH.name = " h";
		NonPiece horEnd = new NonPiece();
		horEnd.name = "  ";

		boardLayout[8][0] = horA;
		boardLayout[8][1] = horB;
		boardLayout[8][2] = horC;
		boardLayout[8][3] = horD;
		boardLayout[8][4] = horE;
		boardLayout[8][5] = horF;
		boardLayout[8][6] = horG;
		boardLayout[8][7] = horH;
		boardLayout[8][8] = horEnd;
	}

	/**
	*prints the game board:
	*/
	public void print(){

		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 9; j++){
				System.out.print(boardLayout[i][j].getName() + " ");
			}
		System.out.println();
		}

	}

	/**
	 * determine game status.
	 * <p>
	 * simulates the threatened squares of each side to determine check,
	 * then looks for available defenses to determine checkmate
	 * <p>
	 * Return strings:
	 * "blackChecked" = black king is checked
	 * "whiteChecked" = white king is checked
	 * "bothChecked" = both kings are checked
	 * "blackCheckMate" = checkmate, black team loses
	 * "whiteCheckMate" = checkmate, white team loses
	 * "bothCheckMate" = checkmate for both sides. STALEMATE.
	 * "noCheck" = no king is checked, proceed as usual
	 * "error" = error, one (or both) of the kings are not found on the board
	 * "White wins" = no checkmate, however the white still won
	 * "Black wins" = no checkmate, however the black still won
	 *
	 * @return the current status of the Kings.
	 */
	public String isKingChecked() {
		Board mainBoard=this;
		int[][] controlBoard = new int[8][8];
		int blackRow = -1;
		int blackCol = -1;
		int whiteRow = -1;
		int whiteCol = -1;
		int whiteAttacker = 0;
		int blackAttacker = 0;
		int blackCollisionNum = 0;
		int whiteCollisionNum = 0;
		boolean whiteCollision = false;
		boolean blackCollision = false;
		boolean whiteChecked = false;
		boolean blackChecked = false;
		boolean whiteCanEscape = false;
		boolean blackCanEscape = false;
		boolean whiteCheckMate = false;
		boolean blackCheckMate = false;
		boolean blackStillHit = false;
		boolean whiteStillHit = false;

		//Copying mainBoard's contents to controlBoard; 0 = no piece, 1 = white, 2 = black

		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){

				if(mainBoard.boardLayout[i][j] instanceof NonPiece){
					//No piece
					controlBoard[i][j] = 0;
				}else{
					if(mainBoard.boardLayout[i][j].getTeam() == false){
						if(mainBoard.boardLayout[i][j] instanceof King){
							//Black King position
							blackRow = i;
							blackCol = j;
							controlBoard[i][j] = 2;
						}else{
							//Black piece
							controlBoard[i][j] = 2;
						}
					}else if(mainBoard.boardLayout[i][j].getTeam() == true){
						if(mainBoard.boardLayout[i][j] instanceof King){
							//White King position
							whiteRow = i;
							whiteCol = j;
							controlBoard[i][j] = 1;
						}else{
							//White piece
							controlBoard[i][j] = 1;
						}
					}
				}
			}
		}

		//Making Sure both Kings can be found on the board:

		if(blackRow == -1 && blackCol == -1){
			return "White wins";
		}else if(whiteRow == -1 && whiteCol == -1){
			return "Black wins";
		}

		//----------------CHECKING WHITE KING----------------

			//projecting black pieces:
			for(int p = 0; p < 8; p++){
				for (int q = 0; q < 8; q++){
					//Identifying black piece:
					if(mainBoard.boardLayout[p][q].getTeam() == false){
						//Projecting black piece:
						whiteCollision = mainBoard.boardLayout[p][q].populateTrajectory(p,  q,  mainBoard, controlBoard);
						if(whiteCollision)
							whiteCollisionNum++;
					}
				}
			}

			//Checking if any of the black pieces can reach the WHITE KING:

			if(whiteCollisionNum > 0)
				whiteChecked=true;

			//Checking if the WHITE KING can escape:

		if(whiteChecked == true){

			if((whiteRow - 1 >= 0) && (whiteCol - 1>=0)){
				if(controlBoard[whiteRow-1][whiteCol-1] != 2
						&& mainBoard.boardLayout[whiteRow-1][whiteCol-1] instanceof NonPiece){
					whiteCanEscape = true;
				}
			}
			if(whiteRow -1 >= 0){
				if(controlBoard[whiteRow-1][whiteCol] != 2
						&& mainBoard.boardLayout[whiteRow-1][whiteCol] instanceof NonPiece){
					whiteCanEscape = true;
				}
			}
			if(whiteRow - 1 >= 0 && whiteCol + 1 < 8){
				if(controlBoard[whiteRow-1][whiteCol+1] != 2
						&& mainBoard.boardLayout[whiteRow-1][whiteCol+1] instanceof NonPiece){
					whiteCanEscape = true;
				}
			}
			if(whiteCol + 1 < 8){
				if(controlBoard[whiteRow][whiteCol+1] != 2
						&& mainBoard.boardLayout[whiteRow][whiteCol+1] instanceof NonPiece){
					whiteCanEscape = true;
				}
			}
			if(whiteRow + 1 < 8 && whiteCol + 1 < 8){
				if(controlBoard[whiteRow+1][whiteCol+1] != 2
						&& mainBoard.boardLayout[whiteRow+1][whiteCol+1] instanceof NonPiece){
					whiteCanEscape = true;
				}
			}
			if(whiteRow + 1 < 8){
				if(controlBoard[whiteRow + 1][whiteCol] != 2
						&& mainBoard.boardLayout[whiteRow+1][whiteCol] instanceof NonPiece){
					whiteCanEscape = true;
				}
			}
			if(whiteRow + 1 < 8 && whiteCol - 1 >= 0){
				if(controlBoard[whiteRow+1][whiteCol-1] != 2
						&& mainBoard.boardLayout[whiteRow+1][whiteCol-1] instanceof NonPiece){
					whiteCanEscape = true;
				}
			}
			if(whiteCol -1 >= 0){
				if(controlBoard[whiteRow][whiteCol-1] != 2
						&& mainBoard.boardLayout[whiteRow][whiteCol-1] instanceof NonPiece){
					whiteCanEscape = true;
				}
			}

		}

		//If WHITE KING is checked and can't escape, checking if other pieces can defend him:

		if(whiteChecked == true && whiteCanEscape == false){

			//Resetting the controlBoard; 0 = no piece, 1 = white, 2 = black

			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){

					if(mainBoard.boardLayout[i][j] instanceof NonPiece){
						//No piece
						controlBoard[i][j] = 0;
					}else{
						if(mainBoard.boardLayout[i][j].getTeam() == false){
							if(mainBoard.boardLayout[i][j] instanceof King){
								//Black King position
								blackRow = i;
								blackCol = j;
								controlBoard[i][j] = 2;
							}else{
								//Black piece
								controlBoard[i][j] = 2;
							}
						}else if(mainBoard.boardLayout[i][j].getTeam() == true){
							if(mainBoard.boardLayout[i][j] instanceof King){
								//White King position
								whiteRow = i;
								whiteCol = j;
								controlBoard[i][j] = 1;
							}else{
								//White piece
								controlBoard[i][j] = 1;
							}
						}
					}
				}
			}

			//Projecting the WHITE defenders to simulate blocking:

			for(int m = 0; m < 8; m++){
				for(int n = 0; n < 8; n++){
					//Identifying white piece:
					if(mainBoard.boardLayout[m][n].getTeam() == true){
						//Projecting white piece:
						mainBoard.boardLayout[m][n].populateTrajectory(m, n, mainBoard, controlBoard);
					}
				}
			}

			//Projecting the BLACK attackers to see if white king is still hit:

			for(int m = 0; m < 8; m++){
				for(int n = 0; n < 8; n++){
					//Identifying black piece:
					if(mainBoard.boardLayout[m][n].getTeam() == false){
						//Projecting black piece:
						whiteCollision = mainBoard.boardLayout[m][n].populateTrajectory(m, n, mainBoard, controlBoard);
						if(whiteCollision){
							whiteStillHit=true;
						}
					}
				}
			}

			//Checking if the WHITE KING is still hit:

			if(whiteStillHit)
				whiteCheckMate=true;

			//If the WHITE KING is not hit, however the number of collisions was 2 or higher, we still have a checkmate:

				if(whiteCheckMate == false && whiteCollisionNum > 1){
					whiteCheckMate = true;
				}
		}

		//----------------CHECKING BLACK KING----------------

			//Resetting the controlBoard; 0 = no piece, 1 = white, 2 = black

			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){

					if(mainBoard.boardLayout[i][j] instanceof NonPiece){
						//No piece
						controlBoard[i][j] = 0;
					}else{
						if(mainBoard.boardLayout[i][j].getTeam() == false){
							if(mainBoard.boardLayout[i][j] instanceof King){
								//Black King position
								blackRow = i;
								blackCol = j;
								controlBoard[i][j] = 2;
							}else{
								//Black piece
								controlBoard[i][j] = 2;
							}
						}else if(mainBoard.boardLayout[i][j].getTeam() == true){
							if(mainBoard.boardLayout[i][j] instanceof King){
								//White King position
								whiteRow = i;
								whiteCol = j;
								controlBoard[i][j] = 1;
							}else{
								//White piece
								controlBoard[i][j] = 1;
							}
						}
					}
				}
			}

			//Checking whether the BLACK KING is checked:

			//projecting white pieces:
			for(int p = 0; p < 8; p++){
				for (int q = 0; q < 8; q++){
					//Identifying white piece:
					if(mainBoard.boardLayout[p][q].getTeam() == true){
						//Projecting white piece:
						blackCollision = mainBoard.boardLayout[p][q].populateTrajectory(p,  q,  mainBoard, controlBoard);
						if(blackCollision)
							blackCollisionNum++;
					}
				}
			}
			//TESTING
			//System.out.println("white attackers: " + blackCollisionNum +"");

			//Checking if any of the white pieces can reach the BLACK KING:
			if(blackCollisionNum>0){
				blackChecked = true;
			}
				//Checking if the BLACK KING can escape:
				if(blackChecked == true){

					if((blackRow - 1 >= 0) && (blackCol - 1>=0)){
						if(controlBoard[blackRow-1][blackCol-1] != 1
								&& (mainBoard.boardLayout[blackRow-1][blackCol-1] instanceof NonPiece)){
							blackCanEscape = true;
						}
					}
					if(blackRow -1 >= 0){
						if(controlBoard[blackRow-1][blackCol] != 1
								&& (mainBoard.boardLayout[blackRow-1][blackCol] instanceof NonPiece)){
							blackCanEscape = true;
						}
					}
					if(blackRow - 1 >= 0 && blackCol + 1 < 8){
						if(controlBoard[blackRow-1][blackCol+1] != 1
								&& (mainBoard.boardLayout[blackRow-1][blackCol+1] instanceof NonPiece)){
							blackCanEscape = true;
						}
					}
					if(blackCol + 1 < 8){
						if(controlBoard[blackRow][blackCol+1] != 1
								&& (mainBoard.boardLayout[blackRow][blackCol+1] instanceof NonPiece)){
							blackCanEscape = true;
						}
					}
					if(blackRow + 1 < 8 && blackCol + 1 < 8){
						if(controlBoard[blackRow+1][blackCol+1] != 1
								&& (mainBoard.boardLayout[blackRow+1][blackCol+1] instanceof NonPiece)){
							blackCanEscape = true;
						}
					}
					if(blackRow + 1 < 8){
						if(controlBoard[blackRow + 1][blackCol] != 1
								&& (mainBoard.boardLayout[blackRow+1][blackCol] instanceof NonPiece)){
							blackCanEscape = true;
						}
					}
					if(blackRow + 1 < 8 && blackCol - 1 >= 0){
						if(controlBoard[blackRow+1][blackCol-1] != 1
								&& (mainBoard.boardLayout[blackRow+1][blackCol-1] instanceof NonPiece)){
							blackCanEscape = true;
						}
					}
					if(blackCol - 1 >= 0){
						if(controlBoard[blackRow][blackCol-1] != 1
								&& (mainBoard.boardLayout[blackRow][blackCol-1] instanceof NonPiece)){
							blackCanEscape = true;
						}
					}

				}

		//If BLACK KING is checked and can't escape, checking if other pieces can defend him:



			if(blackChecked == true && blackCanEscape == false){

				//Resetting the controlBoard; 0 = no piece, 1 = white, 2 = black
				for(int i = 0; i < 8; i++){
					for(int j = 0; j < 8; j++){

						if(mainBoard.boardLayout[i][j] instanceof NonPiece){
							//No piece
							controlBoard[i][j] = 0;
						}else{
							if(mainBoard.boardLayout[i][j].getTeam() == false){
								if(mainBoard.boardLayout[i][j] instanceof King){
									//Black King position
									blackRow = i;
									blackCol = j;
									controlBoard[i][j] = 2;
								}else{
									//Black piece
									controlBoard[i][j] = 2;
								}
							}else if(mainBoard.boardLayout[i][j].getTeam() == true){
								if(mainBoard.boardLayout[i][j] instanceof King){
									//White King position
									whiteRow = i;
									whiteCol = j;
									controlBoard[i][j] = 1;
								}else{
									//White piece
									controlBoard[i][j] = 1;
								}
							}
						}
					}
				}

				//Projecting the BLACK defenders:
				for(int p = 0; p < 8; p++){
					for (int q = 0; q < 8; q++){
						//Identifying black piece:
						if(mainBoard.boardLayout[p][q].getTeam() == false){
							//Projecting black piece:
							mainBoard.boardLayout[p][q].populateTrajectory(p,  q,  mainBoard, controlBoard);
						}
					}
				}

				//Projecting WHITE attackers:
				for(int m = 0; m < 8; m++){
					for(int n = 0; n < 8; n++){
						//Identifying white piece:
						if(mainBoard.boardLayout[m][n].getTeam() == true){
							//Projecting white piece:
							blackCollision = mainBoard.boardLayout[m][n].populateTrajectory(m, n, mainBoard, controlBoard);
							if(blackCollision)
								blackStillHit=true;
						}
					}
				}
				//Checking if BLACK KING is still hit:
				if(blackStillHit){
					blackCheckMate=true;
				}

				//If the BLACK KING is not hit, however the number of collisions was 2 or higher, we still have a checkmate:
				if(blackCheckMate == false && blackCollisionNum > 1){
					//CHECK AGAIN IF BLACK KING CAN MOVE
					blackCheckMate = true;
				}
			}

		//Process return statements:

			if(blackCheckMate == true && whiteCheckMate == true){
				return "bothCheckMate"; //STALEMATE
			}else if(blackCheckMate == true && whiteCheckMate == false){
				return "blackCheckMate"; //Black team loses
			}else if(blackCheckMate == false && whiteCheckMate == true){
				return "whiteCheckMate"; //White team loses
			}else if(blackCheckMate == false && whiteCheckMate == false){
				if(blackChecked == true && whiteChecked == true){
					return "bothChecked"; //Both kings are under check
				}else if(blackChecked == true && whiteChecked == false){
					return "blackChecked";
				}else if(blackChecked == false && whiteChecked == true){
					return "whiteChecked";
				}else if(blackChecked == false && whiteChecked == false){
					return "noCheck"; //Everything is fine.
				}
			}
		return "noCheck";
	}
}
